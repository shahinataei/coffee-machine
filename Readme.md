## Introduction

#### Coffee machine backend challenge

The goal of this exercise is to try to understand your code skills, architectural decisions and also the overall problem-solving

I use some of my boilerplate like dockerfile and config with docker-compose structure that already created by myself

## Stack & technology used

- PHP 8.1
- Symfony 6.2
- Docker
- Docker compose
- Composer
- RestApi

## Why most of the classes are final?

You can read this [article](https://matthiasnoback.nl/2018/09/final-classes-by-default-why/) to understand why I prefer to use final classes and methods

## Installation
- Clone the project
- Run `docker compose up --build`

## Usage

you can access to api from the `http://localhost:6002/api/v1/`
- `http://localhost:6002/api/v1/espresso/{id}` - `POST` - `make espresso` if you do not provide the id, it will be generated automatically
 then you can mutate same state by that unique id
- `http://localhost:6002/api/v1/doubleespresso/{id}` - `POST` - `make double espresso` if you do not provide the id, it will be generated automatically
  then you can mutate same state by that unique id
- `http://localhost:6002/api/v1/espressomachinestatus/{id}` - `GET` - `status of the machine` if you do not provide the id, it will be generated automatically
  then you can mutate same state by that unique id

## Tests

- `make test` - run all tests
- `make test-unit` - run unit tests
- `make test-integration` - run integration tests
- `make test-ui` - run user interface tests

## Shell access

- `make shell` - access to the container


## Debugging

if you face to this error `Error: No such container: coffee-machine-soconnect-php-service` while running `make shell` command or other commands
you can use the `docker container ls` and find match container then replace `coffee-machine-soconnect-php-service` on line 4 of the `Makefile` with the container name