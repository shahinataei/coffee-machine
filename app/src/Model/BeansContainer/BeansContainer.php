<?php

declare(strict_types=1);

namespace App\Model\BeansContainer;

use App\Model\CoffeeMachine\ContainerFullException;

final class BeansContainer implements BeansContainerInterface
{
    private int $numSpoons;
    private readonly int $maxSpoons;

    public function __construct(int $numSpoons)
    {
        $this->numSpoons = $numSpoons;
        $this->maxSpoons = $numSpoons;
    }

    public function getBeans(): int
    {
        return $this->numSpoons;
    }

    public function addBeans(int $numSpoons): void
    {
        if ($this->numSpoons + $numSpoons > $this->maxSpoons) {
            throw new ContainerFullException('Too many beans in the container.');
        }
        $this->numSpoons += $numSpoons;
    }

    /**
     * @throws NoBeansException
     */
    public function useBeans(int $numSpoons): int
    {
        if ($this->numSpoons < $numSpoons) {
            throw new NoBeansException('Not enough beans in the container.');
        }

        $this->numSpoons -= $numSpoons;

        return $numSpoons;
    }
}
