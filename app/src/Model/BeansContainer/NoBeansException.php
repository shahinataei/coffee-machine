<?php

declare(strict_types=1);

namespace App\Model\BeansContainer;

use App\Model\CoffeeMachine\EspressoMachineException;

class NoBeansException extends EspressoMachineException
{
}
