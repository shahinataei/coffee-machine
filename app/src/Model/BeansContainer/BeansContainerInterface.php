<?php

declare(strict_types=1);

namespace App\Model\BeansContainer;

use App\Model\CoffeeMachine\ContainerFullException;

interface BeansContainerInterface
{
    /**
     * Adds beans to the container.
     *
     * @param int $numSpoons number of spoons of beans
     *
     * @throws ContainerFullException
     */
    public function addBeans(int $numSpoons): void;

    /**
     * Use $numSpoons from the container.
     *
     * @param int $numSpoons number of spoons of beans
     *
     * @return int number of bean spoons used
     */
    public function useBeans(int $numSpoons): int;

    /**
     * Returns the number of spoons of beans left in the container.
     */
    public function getBeans(): int;
}
