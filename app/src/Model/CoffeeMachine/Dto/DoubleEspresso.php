<?php

declare(strict_types=1);

namespace App\Model\CoffeeMachine\Dto;

final class DoubleEspresso
{
    public function __construct(
        public readonly float $waterLiters,
        public readonly int $numSpoons,
    ) {
    }
}
