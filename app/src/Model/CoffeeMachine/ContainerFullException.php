<?php

declare(strict_types=1);

namespace App\Model\CoffeeMachine;

use App\Model\ContainerException;

class ContainerFullException extends ContainerException
{
}
