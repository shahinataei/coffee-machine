<?php

declare(strict_types=1);

namespace App\Model\CoffeeMachine;

use App\Model\BeansContainer\BeansContainerInterface;
use App\Model\WaterContainer\WaterContainerInterface;

abstract class CoffeeMachine
{
    protected string $status;

    public function __construct(
        public readonly string $id,
        public readonly WaterContainerInterface $waterContainer,
        public readonly BeansContainerInterface $beansContainer,
    ) {
    }

    abstract protected function initialStatus(): void;

    public function getStatus(): string
    {
        return $this->status;
    }
}
