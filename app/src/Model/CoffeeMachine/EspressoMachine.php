<?php

declare(strict_types=1);

namespace App\Model\CoffeeMachine;

use App\Model\BeansContainer\BeansContainerInterface;
use App\Model\BeansContainer\NoBeansException;
use App\Model\CoffeeMachine\Dto\DoubleEspresso;
use App\Model\CoffeeMachine\Dto\Espresso;
use App\Model\ContainerException;
use App\Model\WaterContainer\NoWaterException;
use App\Model\WaterContainer\WaterContainerInterface;

final class EspressoMachine extends CoffeeMachine implements EspressoMachineInterface
{
    public function __construct(
        string $id,
        WaterContainerInterface $waterContainer,
        BeansContainerInterface $beansContainer,
        public readonly Espresso $espresso,
        public readonly DoubleEspresso $doubleEspresso,
    ) {
        parent::__construct($id, $waterContainer, $beansContainer);
        $this->initialStatus();
    }

    /**
     * @throws ContainerException
     */
    public function makeEspresso(): float
    {
        try {
            $this->waterContainer->useWater($this->espresso->waterLiters);
            $this->beansContainer->useBeans($this->espresso->numSpoons);
        } catch (NoWaterException|NoBeansException $e) {
            $this->status = $this->calculateStatus($this->espresso->waterLiters, $this->espresso->numSpoons);
            throw new ContainerException($e->getMessage());
        }

        $this->status = $this->calculateStatus($this->espresso->waterLiters, $this->espresso->numSpoons);

        return $this->espresso->waterLiters;
    }

    /**
     * @throws ContainerException
     */
    public function makeDoubleEspresso(): float
    {
        try {
            $this->waterContainer->useWater($this->doubleEspresso->waterLiters);
            $this->beansContainer->useBeans($this->doubleEspresso->numSpoons);
        } catch (NoWaterException|NoBeansException $e) {
            $this->status = $this->calculateStatus($this->doubleEspresso->waterLiters, $this->doubleEspresso->numSpoons);
            throw new ContainerException($e->getMessage());
        }

        $this->status = $this->calculateStatus($this->doubleEspresso->waterLiters, $this->doubleEspresso->numSpoons);

        return $this->doubleEspresso->waterLiters;
    }

    protected function initialStatus(): void
    {
        $this->status = $this->calculateRemainEspresso().' Espressos left';
    }

    private function calculateRemainEspresso(): int|float
    {
        $remainWater = \round($this->waterContainer->getWater() / $this->espresso->waterLiters);
        $remainBeans = $this->beansContainer->getBeans() / $this->espresso->numSpoons;

        return (int) \min($remainWater, $remainBeans);
    }

    private function calculateStatus(float $neededWater, int $neededNumSpoons): string
    {
        $water = $this->waterContainer->getWater();
        $numSpoons = $this->beansContainer->getBeans();

        if ($water < $neededWater && $numSpoons < $neededNumSpoons) {
            return 'Add beans and water';
        }

        if ($water < $neededWater) {
            return 'Add water';
        }

        if ($numSpoons < $neededNumSpoons) {
            return 'Add beans';
        }

        return $this->calculateRemainEspresso().' Espressos left';
    }
}
