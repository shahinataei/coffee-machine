<?php

declare(strict_types=1);

namespace App\Model\CoffeeMachine;

abstract class EspressoMachineException extends \Exception
{
}
