<?php

declare(strict_types=1);

namespace App\Model\WaterContainer;

use App\Model\CoffeeMachine\ContainerFullException;

final class WaterContainer implements WaterContainerInterface
{
    private float $water;
    private readonly float $maxWater;

    public function __construct(float $water)
    {
        $this->water = $water;
        $this->maxWater = $water;
    }

    public function getWater(): float
    {
        return $this->water;
    }

    public function addWater(float $litres): void
    {
        if ($this->water + $litres > $this->maxWater) {
            throw new ContainerFullException('Too much water in the container.');
        }
        $this->water += $litres;
    }

    /**
     * @throws NoWaterException
     */
    public function useWater(float $litres): float
    {
        if ($this->water < $litres) {
            throw new NoWaterException('Not enough water in the container.');
        }

        $this->water = round($this->water - $litres, 2);

        return $litres;
    }
}
