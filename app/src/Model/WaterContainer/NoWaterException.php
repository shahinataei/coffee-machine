<?php

declare(strict_types=1);

namespace App\Model\WaterContainer;

use App\Model\CoffeeMachine\EspressoMachineException;

class NoWaterException extends EspressoMachineException
{
}
