<?php

declare(strict_types=1);

namespace App\Service;

use App\Model\CoffeeMachine\CoffeeMachine;
use App\Model\CoffeeMachine\EspressoMachineInterface;

final class CoffeeMaker
{
    private array $coffeeMachines;

    public function __construct(
        CoffeeMachine $coffeeMachineFactory,
    ) {
        $this->coffeeMachines[$coffeeMachineFactory::class] = $coffeeMachineFactory;
    }

    /**
     * @throws CoffeeMachineNotFoundException
     */
    public function getEspressoMachine(): ?EspressoMachineInterface
    {
        $interface = EspressoMachineInterface::class;
        foreach ($this->coffeeMachines as $coffeeMachine) {
            if (\in_array($interface, \class_implements($coffeeMachine))) {
                return $coffeeMachine;
            }
        }

        throw new CoffeeMachineNotFoundException('No coffee machine found');
    }
}
