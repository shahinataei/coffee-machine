<?php

declare(strict_types=1);

namespace App\UserInterface\Rest\Controller;

use App\Infrastructure\CoffeeMachineHistoryRepository;
use App\Infrastructure\CoffeeMachineHistoryRepositoryInterface;
use App\Model\BeansContainer\BeansContainer;
use App\Model\CoffeeMachine\Dto\DoubleEspresso;
use App\Model\CoffeeMachine\Dto\Espresso;
use App\Model\CoffeeMachine\EspressoMachine;
use App\Model\CoffeeMachine\EspressoMachineInterface;
use App\Model\WaterContainer\WaterContainer;
use App\Service\CoffeeMachineNotFoundException;
use App\Service\CoffeeMaker;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

final class CoffeeMachineController extends AbstractController
{
    private readonly EspressoMachineInterface $espressoMachine;
    private readonly CoffeeMachineHistoryRepositoryInterface $repository;
    private array $jsonResponse = [];

    #[Route('/api/v1/espresso/{id}', name: 'make_one_espresso', defaults: ['id' => ''], methods: ['POST']), ]
    public function MakeOneEspresso(string $id): JsonResponse
    {
        try {
            $id = $this->coffeeMachineBuilder($id);
            $this->espressoMachine->makeEspresso();
            $this->repository->storeEspressoCoffeeMachineHistory(
                $this->espressoMachine,
                $this->espressoMachine->espresso->waterLiters,
                $this->espressoMachine->espresso->numSpoons,
            );
        } catch (\Exception $e) {
            return new JsonResponse(
                [
                    'exception' => $e->getMessage(),
                ], 400
            );
        }

        return $this->json(
            [
                [
                    'status' => $this->espressoMachine->getStatus(),
                    'id' => $id,
                    ...$this->jsonResponse
                ],
            ]
        );
    }

    #[Route('/api/v1/doubleespresso/{id}', name: 'make_double_espresso', defaults: ['id' => ''], methods: ['POST']), ]
    public function MakeOneDoubleEspresso(string $id): JsonResponse
    {
        try {
            $id = $this->coffeeMachineBuilder($id);
            $this->espressoMachine->makeDoubleEspresso();
            $this->repository->storeEspressoCoffeeMachineHistory(
                $this->espressoMachine,
                $this->espressoMachine->doubleEspresso->waterLiters,
                $this->espressoMachine->doubleEspresso->numSpoons,
            );
        } catch (\Exception $e) {
            return new JsonResponse(
                [
                    'exception' => $e->getMessage(),
                ], 400
            );
        }

        return $this->json(
            [
                [
                    'status' => $this->espressoMachine->getStatus(),
                    'id' => $id,
                    ...$this->jsonResponse
                ],
            ]
        );
    }

    #[Route('/api/v1/espressomachinestatus/{id}', name: 'make_one_double_espresso', defaults: ['id' => ''], methods: ['GET']), ]
    public function StatusOfEspressoMachine(string $id): JsonResponse
    {
        try {
            $id = $this->coffeeMachineBuilder($id);
            $this->repository->storeEspressoCoffeeMachineHistory(
                $this->espressoMachine,
                $this->espressoMachine->doubleEspresso->waterLiters,
                $this->espressoMachine->doubleEspresso->numSpoons,
            );
        } catch (\Exception $e) {
            return new JsonResponse(
                [
                    'exception' => $e->getMessage(),
                ], 400
            );
        }

        return $this->json(
            [
                [
                    'status' => $this->espressoMachine->getStatus(),
                    'id' => $id,
                    ...$this->jsonResponse
                ],
            ]
        );
    }

    private function coffeeMachineBuilder(string $id): string|JsonResponse
    {
        try {
            if (empty($id)) {
                $id = Uuid::v4()->toRfc4122();
            }
            $storeFile = dirname(__DIR__, 4) . '/storage/tmp-db.json';
            $this->repository = new CoffeeMachineHistoryRepository($storeFile);
            $waterContainer = new WaterContainer(2.0);
            $beansContainer = new BeansContainer(50);
            $espresso = new Espresso(0.05, 1);
            $doubleEspresso = new DoubleEspresso(0.1, 2);
            $espressoMachine = new EspressoMachine($id, $waterContainer, $beansContainer, $espresso, $doubleEspresso);
            $espressoMachine = $this->repository->getEspressoCoffeeMachine($espressoMachine);
            $coffeeMaker = new CoffeeMaker($espressoMachine);
            $this->espressoMachine = $coffeeMaker->getEspressoMachine();
            $this->jsonResponse = [
                'water-container' => $waterContainer->getWater(),
                'beans-container' => $beansContainer->getBeans(),
                'espresso-water-liter' => $espresso->waterLiters,
                'espresso-num-spoons' => $espresso->numSpoons,
                'double-espresso-water-liter' => $doubleEspresso->waterLiters,
                'double-espresso-num-spoons' => $doubleEspresso->numSpoons
            ];
            return $id;
        } catch (CoffeeMachineNotFoundException $e) {
            return new JsonResponse(
                [
                    'exception' => $e->getMessage(),
                ], 400
            );
        }
    }
}
