<?php

declare(strict_types=1);

namespace App\Infrastructure;

use App\Model\CoffeeMachine\EspressoMachine;

interface CoffeeMachineHistoryRepositoryInterface
{
    public function storeEspressoCoffeeMachineHistory(EspressoMachine $espressoMachine, float $usedWater, int $usedNumSpoon): void;

    public function getEspressoCoffeeMachine(EspressoMachine $espressoMachine): EspressoMachine;
}
