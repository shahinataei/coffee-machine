<?php

declare(strict_types=1);

namespace App\Infrastructure;

use App\Model\CoffeeMachine\EspressoMachine;

final class CoffeeMachineHistoryRepository implements CoffeeMachineHistoryRepositoryInterface
{
    public function __construct(private readonly string $file)
    {
        if (!\file_exists($file)) {
            \file_put_contents($file, '[]');
        }
    }

    public function storeEspressoCoffeeMachineHistory(EspressoMachine $espressoMachine, float $usedWater, int $usedNumSpoon): void
    {
        $history = \json_decode(\file_get_contents($this->file), true);
        $history[$espressoMachine->id][] = [
            'water' => $usedWater,
            'beans' => $usedNumSpoon,
            'status' => $espressoMachine->getStatus(),
        ];
        \file_put_contents($this->file, \json_encode($history));
    }

    public function getEspressoCoffeeMachine(EspressoMachine $espressoMachine): EspressoMachine
    {
        $history = \json_decode(\file_get_contents($this->file), true);
        if (isset($history[$espressoMachine->id])) {
            foreach ($history[$espressoMachine->id] as $espressoShot) {
                $espressoMachine->waterContainer->useWater($espressoShot['water']);
                $espressoMachine->beansContainer->useBeans($espressoShot['beans']);
            }
        }

        return $espressoMachine;
    }
}
