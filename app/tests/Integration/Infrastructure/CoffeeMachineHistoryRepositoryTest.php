<?php

declare(strict_types=1);

namespace App\Tests\Integration\Infrastructure;

use App\Infrastructure\CoffeeMachineHistoryRepository;
use App\Model\BeansContainer\BeansContainer;
use App\Model\CoffeeMachine\Dto\DoubleEspresso;
use App\Model\CoffeeMachine\Dto\Espresso;
use App\Model\CoffeeMachine\EspressoMachine;
use App\Model\WaterContainer\WaterContainer;
use App\Service\CoffeeMachineNotFoundException;
use App\Service\CoffeeMaker;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

final class CoffeeMachineHistoryRepositoryTest extends TestCase
{
    /**
     * @throws CoffeeMachineNotFoundException
     */
    public function test_it_should_success_store_coffee_machine_history_and_return_that(): void
    {
        $id = Uuid::v4()->toRfc4122();
        $storeFile = dirname(__DIR__, 3) . '/storage/tmp-test-db.json';
        $repository = new CoffeeMachineHistoryRepository($storeFile);
        $waterContainer = new WaterContainer(2.0);
        $beansContainer = new BeansContainer(50);
        $espresso = new Espresso(0.05, 1);
        $doubleEspresso = new DoubleEspresso(0.1, 2);
        $espressoMachine = new EspressoMachine($id, $waterContainer, $beansContainer, $espresso, $doubleEspresso);
        $coffeeMaker = new CoffeeMaker($espressoMachine);
        $espressoMachine = $coffeeMaker->getEspressoMachine();
        $repository->storeEspressoCoffeeMachineHistory(
            $espressoMachine,
            $espressoMachine->doubleEspresso->waterLiters,
            $espressoMachine->doubleEspresso->numSpoons,
        );

        $espressoMachineStored = $repository->getEspressoCoffeeMachine($espressoMachine);

        $this->assertEquals($espressoMachine->getStatus(), $espressoMachineStored->getStatus());
    }
}