<?php

declare(strict_types=1);

namespace App\Tests\Ui\Rest\Controller;

use App\Tests\Ui\UiTestCase;

final class CoffeeMachineControllerTest extends UiTestCase
{
    public function test_success_make_espresso_by_rest_api(): void
    {
        $this->client->request('POST', '/api/v1/espresso');

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertJson($this->client->getResponse()->getContent());

        $response = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('status', $response[0]);
        $this->assertArrayHasKey('id', $response[0]);
        $this->assertArrayHasKey('water-container', $response[0]);
        $this->assertArrayHasKey('beans-container', $response[0]);
        $this->assertArrayHasKey('beans-container', $response[0]);
        $this->assertArrayHasKey('espresso-num-spoons', $response[0]);

        $waterContainer = (float) $response[0]['water-container'];
        $beansContainer = $response[0]['beans-container'];
        $espressoWaterLiter = (float) $response[0]['espresso-water-liter'];
        $espressoNumSpoons = $response[0]['espresso-num-spoons'];

        $waterContainer = $waterContainer-$espressoWaterLiter;
        $beansContainer = $beansContainer-$espressoNumSpoons;

        $remainWater = round($waterContainer / $espressoWaterLiter);
        $remainBeans = $beansContainer / $espressoNumSpoons;

        $espressoRemainCount = ((int) \min($remainWater, $remainBeans));
        $espressoRemainCountHold = $espressoRemainCount;

        for($i = 0; $i < $espressoRemainCount-1; $i++) {
            $espressoRemainCountHold--;
            $this->client->request('POST', '/api/v1/espresso/' . $response[0]['id']);
            $newResponse = json_decode($this->client->getResponse()->getContent(), true);
            $this->assertEquals($espressoRemainCountHold . ' Espressos left', $newResponse[0]['status']);
        }

        $this->client->request('POST', '/api/v1/espresso/' . $response[0]['id']);
        $newResponse = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('Add water', $newResponse[0]['status']);
    }

    public function test_success_make_double_espresso_by_rest_api(): void
    {
        $this->client->request('POST', '/api/v1/doubleespresso');

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertJson($this->client->getResponse()->getContent());

        $response = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('status', $response[0]);
        $this->assertArrayHasKey('id', $response[0]);
        $this->assertArrayHasKey('water-container', $response[0]);
        $this->assertArrayHasKey('beans-container', $response[0]);
        $this->assertArrayHasKey('beans-container', $response[0]);
        $this->assertArrayHasKey('espresso-num-spoons', $response[0]);

        $waterContainer = (float) $response[0]['water-container'];
        $beansContainer = $response[0]['beans-container'];
        $doubleEspressoWaterLiter = (float) $response[0]['double-espresso-water-liter'];
        $doubleEspressoNumSpoons = $response[0]['double-espresso-num-spoons'];

        $waterContainer = $waterContainer-$doubleEspressoWaterLiter;
        $beansContainer = $beansContainer-$doubleEspressoNumSpoons;

        $remainWater = round($waterContainer / $doubleEspressoWaterLiter);
        $remainBeans = $beansContainer / $doubleEspressoNumSpoons;

        $espressoRemainCount = ((int) \min($remainWater, $remainBeans));
        $espressoRemainCountHold = $espressoRemainCount;

        for($i = 0; $i < $espressoRemainCount-1; $i++) {
            $espressoRemainCountHold--;
            $this->client->request('POST', '/api/v1/doubleespresso/' . $response[0]['id']);
            $newResponse = json_decode($this->client->getResponse()->getContent(), true);

            $this->assertEquals(($espressoRemainCountHold * 2) . ' Espressos left', $newResponse[0]['status']);
        }

        $this->client->request('POST', '/api/v1/doubleespresso/' . $response[0]['id']);
        $newResponse = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('Add water', $newResponse[0]['status']);
    }

    public function test_success_status_espresso_by_rest_api(): void
    {
        $this->client->request('GET', '/api/v1/espressomachinestatus');

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertJson($this->client->getResponse()->getContent());

        $response = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('status', $response[0]);
        $this->assertArrayHasKey('id', $response[0]);
        $this->assertArrayHasKey('water-container', $response[0]);
        $this->assertArrayHasKey('beans-container', $response[0]);
        $this->assertArrayHasKey('beans-container', $response[0]);
        $this->assertArrayHasKey('espresso-num-spoons', $response[0]);

        $waterContainer = (float) $response[0]['water-container'];
        $beansContainer = $response[0]['beans-container'];
        $espressoWaterLiter = (float) $response[0]['espresso-water-liter'];
        $espressoNumSpoons = $response[0]['espresso-num-spoons'];

        $remainWater = $waterContainer / $espressoWaterLiter;
        $remainBeans = $beansContainer / $espressoNumSpoons;

        $espressoRemainCount = (int) \min($remainWater, $remainBeans);

        $this->assertEquals($espressoRemainCount . ' Espressos left', $response[0]['status']);
    }
}