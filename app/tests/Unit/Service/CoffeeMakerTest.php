<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service;

use App\Model\BeansContainer\BeansContainer;
use App\Model\CoffeeMachine\Dto\DoubleEspresso;
use App\Model\CoffeeMachine\Dto\Espresso;
use App\Model\CoffeeMachine\EspressoMachine;
use App\Model\WaterContainer\WaterContainer;
use App\Service\CoffeeMachineNotFoundException;
use App\Service\CoffeeMaker;
use App\Tests\Stub\CoffeeMachine\StubCoffeeMachine;
use PHPUnit\Framework\TestCase;

final class CoffeeMakerTest extends TestCase
{
    public function test_get_EspressoMachine(): void
    {
        $id = '123';
        $waterContainer = new WaterContainer(2.0);
        $beansContainer = new BeansContainer(50);
        $espresso = new Espresso(0.05, 1);
        $doubleEspresso = new DoubleEspresso(0.1, 2);
        $espressoMachine = new EspressoMachine($id, $waterContainer, $beansContainer, $espresso, $doubleEspresso);
        $coffeeMaker = new CoffeeMaker($espressoMachine);
        $this->assertEquals($espressoMachine, $coffeeMaker->getEspressoMachine());
    }

    public function test_get_EspressoMachine_with_exception(): void
    {
        $this->expectException(CoffeeMachineNotFoundException::class);
        $this->expectExceptionMessage('No coffee machine found');
        $id = '123';
        $waterContainer = new WaterContainer(2.0);
        $beansContainer = new BeansContainer(50);
        $stubCoffeeMachine = new StubCoffeeMachine($id, $waterContainer, $beansContainer);
        $coffeeMaker = new CoffeeMaker($stubCoffeeMachine);
        $coffeeMaker->getEspressoMachine();
    }
}