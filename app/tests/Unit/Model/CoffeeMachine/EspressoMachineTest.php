<?php

declare(strict_types=1);

namespace App\Tests\Unit\Model\CoffeeMachine;

use App\Model\BeansContainer\BeansContainer;
use App\Model\CoffeeMachine\Dto\DoubleEspresso;
use App\Model\CoffeeMachine\Dto\Espresso;
use App\Model\CoffeeMachine\EspressoMachine;
use App\Model\ContainerException;
use App\Model\WaterContainer\WaterContainer;
use PHPUnit\Framework\TestCase;

final class EspressoMachineTest extends TestCase
{
    private readonly EspressoMachine $espressoMachine;

    public function setup(): void
    {
        $id = '123';
        $waterContainer = new WaterContainer(2.0);
        $beansContainer = new BeansContainer(50);
        $espresso = new Espresso(0.05, 1);
        $doubleEspresso = new DoubleEspresso(0.1, 2);
        $this->espressoMachine = new EspressoMachine($id, $waterContainer, $beansContainer, $espresso, $doubleEspresso);
    }

    public function test_make_espresso(): void
    {
        $espressoVolume = $this->espressoMachine->makeEspresso();
        $this->assertEquals(0.05, $espressoVolume);
        $this->assertEquals(1.95, $this->espressoMachine->waterContainer->getWater());
        $this->assertEquals(49, $this->espressoMachine->beansContainer->getBeans());
    }

    public function test_make_double_espresso(): void
    {
        $espressoVolume = $this->espressoMachine->makeDoubleEspresso();
        $this->assertEquals(0.1, $espressoVolume);
        $this->assertEquals(1.9, $this->espressoMachine->waterContainer->getWater());
        $this->assertEquals(48, $this->espressoMachine->beansContainer->getBeans());
    }

    public function test_get_initial_status(): void
    {
        $status = $this->espressoMachine->getStatus();
        $this->assertEquals('40 Espressos left', $status);
    }

    public function test_get_status_after_40_espressos(): void
    {
        for ($i = 0; $i < 40; $i++) {
            $this->espressoMachine->makeEspresso();
        }
        $status = $this->espressoMachine->getStatus();
        $this->assertEquals('Add water', $status);
    }

    public function test_get_status_after_20_double_espressos(): void
    {
        for ($i = 0; $i < 20; $i++) {
            $this->espressoMachine->makeDoubleEspresso();
        }
        $status = $this->espressoMachine->getStatus();
        $this->assertEquals('Add water', $status);
    }

    public function test_get_container_exception_when_water_is_not_enough(): void
    {
        $this->expectException(ContainerException::class);
        $this->expectExceptionMessage('Not enough water in the container.');
        for ($i = 0; $i < 21; $i++) {
            $this->espressoMachine->makeDoubleEspresso();
        }
    }

    public function test_get_container_exception_when_beans_are_not_enough(): void
    {
        $this->expectException(ContainerException::class);
        $this->expectExceptionMessage('Not enough beans in the container.');
        $id = '123';
        $waterContainer = new WaterContainer(4.0);
        $beansContainer = new BeansContainer(50);
        $espresso = new Espresso(0.05, 1);
        $doubleEspresso = new DoubleEspresso(0.1, 2);
        $espressoMachine = new EspressoMachine($id, $waterContainer, $beansContainer, $espresso, $doubleEspresso);
        for ($i = 0; $i < 51; $i++) {
            $espressoMachine->makeEspresso();
        }
    }

    public function test_get_status_when_beans_container_finished(): void
    {
        $id = '123';
        $waterContainer = new WaterContainer(4.0);
        $beansContainer = new BeansContainer(50);
        $espresso = new Espresso(0.05, 1);
        $doubleEspresso = new DoubleEspresso(0.1, 2);
        $espressoMachine = new EspressoMachine($id, $waterContainer, $beansContainer, $espresso, $doubleEspresso);
        for ($i = 0; $i < 50; $i++) {
            $espressoMachine->makeEspresso();
        }
        $this->assertEquals('Add beans', $espressoMachine->getStatus());
    }

    public function test_get_status_when_beans_and_water_need(): void
    {
        $id = '123';
        $waterContainer = new WaterContainer(2.0);
        $beansContainer = new BeansContainer(40);
        $espresso = new Espresso(0.05, 1);
        $doubleEspresso = new DoubleEspresso(0.1, 2);
        $espressoMachine = new EspressoMachine($id, $waterContainer, $beansContainer, $espresso, $doubleEspresso);
        for ($i = 0; $i < 40; $i++) {
            $espressoMachine->makeEspresso();
        }
        $this->assertEquals('Add beans and water', $espressoMachine->getStatus());
    }
}