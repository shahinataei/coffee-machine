<?php

declare(strict_types=1);

namespace App\Tests\Unit\Model\WaterContainer;

use App\Model\CoffeeMachine\ContainerFullException;
use App\Model\WaterContainer\NoWaterException;
use App\Model\WaterContainer\WaterContainer;
use PHPUnit\Framework\TestCase;

final class WaterContainerTest extends TestCase
{
    public function test_it_can_be_created(): void
    {
        $waterContainer = new WaterContainer(10);
        $this->assertEquals(10, $waterContainer->getWater());
    }

    public function test_it_can_add_water(): void
    {
        $waterContainer = new WaterContainer(10);
        $waterContainer->useWater(5);
        $waterContainer->addWater(5);
        $this->assertEquals(10, $waterContainer->getWater());
    }

    public function test_it_can_use_water(): void
    {
        $waterContainer = new WaterContainer(10);
        $waterContainer->useWater(5);
        $this->assertEquals(5, $waterContainer->getWater());
    }

    public function test_it_should_be_throw_exception_when_add_water_mor_than_container_capacity(): void
    {
        $waterContainer = new WaterContainer(10);
        $this->expectException(ContainerFullException::class);
        $this->expectExceptionMessage('Too much water in the container.');
        $waterContainer->addWater(5);
    }

    public function test_it_should_be_throw_exception_when_use_water_mor_than_container_capacity(): void
    {
        $waterContainer = new WaterContainer(10);
        $this->expectException(NoWaterException::class);
        $this->expectExceptionMessage('Not enough water in the container.');
        $waterContainer->useWater(15);
    }
}