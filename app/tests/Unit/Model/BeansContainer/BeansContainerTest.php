<?php

declare(strict_types=1);

namespace App\Tests\Unit\Model\BeansContainer;

use App\Model\BeansContainer\BeansContainer;
use App\Model\BeansContainer\NoBeansException;
use App\Model\CoffeeMachine\ContainerFullException;
use PHPUnit\Framework\TestCase;

final class BeansContainerTest extends TestCase
{
    public function test_it_can_be_created(): void
    {
        $beansContainer = new BeansContainer(10);
        $this->assertEquals(10, $beansContainer->getBeans());
    }

    public function test_it_can_add_use_beans(): void
    {
        $beansContainer = new BeansContainer(10);
        $beansContainer->useBeans(5);
        $this->assertEquals(5, $beansContainer->getBeans());
    }

    public function test_it_can_add_beans(): void
    {
        $beansContainer = new BeansContainer(15);
        $beansContainer->useBeans(7);
        $beansContainer->addBeans(5);
        $this->assertEquals(13, $beansContainer->getBeans());
    }

    public function test_it_should_be_throw_exception_when_add_beans_mor_than_container_capacity(): void
    {
        $beansContainer = new BeansContainer(10);
        $this->expectException(ContainerFullException::class);
        $this->expectExceptionMessage('Too many beans in the container.');
        $beansContainer->addBeans(5);
    }

    public function test_it_should_be_throw_exception_when_use_beans_mor_than_container_capacity(): void
    {
        $beansContainer = new BeansContainer(10);
        $this->expectException(NoBeansException::class);
        $this->expectExceptionMessage('Not enough beans in the container.');
        $beansContainer->useBeans(15);
    }
}