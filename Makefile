.DEFAULT_GOAL := help

SHELL := /bin/bash
COMPOSE := docker exec -it coffee-machine-soconnect-php-service
APP := $(COMPOSE)
NPROC := `nproc`

##@ Setup

.PHONY: cs-fix
cs-fix: ## Auto-fixes any style related code violations
	$(APP) vendor/bin/php-cs-fixer fix src
	$(APP) vendor/bin/php-cs-fixer fix tests

.PHONY: shell
shell: ## Provides shell access to the running PHP container instance
	$(COMPOSE) /bin/sh

.PHONY: test-ui
test-ui:
	$(APP) bin/phpunit --verbose --testsuite=ui

.PHONY: test-unit
test-unit:
	$(APP) bin/phpunit --verbose --testsuite=unit

.PHONY: test-integration
test-integration:
	$(APP) bin/phpunit --verbose --testsuite=integration

.PHONY: test
test:
	$(APP) bin/phpunit --verbose